const {
  findMatches,
  getRelativesMap,
  parseData
} = require('./helpers');
// const { relativesMarkers, wikiPage } = require('./__mock__/test');


const getRelativesNames = (string, markers) => {
  try {
    const relativesMap = getRelativesMap(markers);
    const relativesData = findMatches(string, relativesMap);
    const parsedRelativesData = parseData(relativesData);

    return parsedRelativesData;
  } catch (err) {
    console.log(err.message);
  }
};

// console.log(getRelativesNames(wikiPage, relativesMarkers));


module.exports = getRelativesNames;