# wiki_relatives_parser

## Install

>npm install https://Reinegardie@bitbucket.org/Reinegardie/wiki_relatives_parser.git

## Code Example

```javascript
const getRelatives = require('wiki_relatives_parser');
// import getRelatives from 'wiki_relatives_parser';

const relatives = getRelatives(pageContent, ['children', 'parents', 'spouse']);
```

The result will be like:
```javascript
{
   children: ["Damien Wayne"],
   parents: ["Thomas Wayne", "Martha Wayne"],
   spouse: ["Talia al'Ghul"]
}
```

This is an **alpha** version of the module.
It's purpose is to investigate the ways to parse wiki pages content, returned by ```https://en.wikipedia.org/w/api/php```
**It works only with several pages right now**