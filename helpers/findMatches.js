module.exports = (string, map) => {
  if (!string || typeof string !== 'string' || !string.length) throw new Error('findMatches. Invalid or empty string argument');
  if (!map || typeof map !== 'object' || !Object.keys(map).length) throw new Error('findMatches. Invalid or empty map argument');

  return Object.entries(map).reduce((acc, markerPair) => {
    const [marker, regex] = markerPair;
    const targetSubString = string.match(regex);
  
    if (!targetSubString || !targetSubString[1]) return acc;
  
    acc[marker] = targetSubString[1];
  
    return acc;
  }, {});
};