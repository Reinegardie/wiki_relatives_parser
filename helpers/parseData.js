const regex = {
  'asteriks': /\*([^*])+/g,
  'hasSquareBrackets': /\[\[(.+)\]\]/,
  'hlist': /^{{hlist/,
  'name': /([A-Z][a-z]+\s?)+/,
  'plainList': /^{{plain list/,
  'squareBracketsFull': /\[\[([a-z\s.|]+)\]\]/gi,
  'squareBracketsName': /^\[\[([^|\]]+)/
};

const parsers = {
  'hlist': (string) => {
    if (!string || typeof string !== 'string' || !string.length) throw new Error('hlist parser. Invalid or empty string argument');

    const matches = string.match(regex.squareBracketsFull);
    const names = matches.reduce((acc, fullString) => {
      const name = fullString.match(regex.squareBracketsName);

      if (!name || !name[1]) return acc;

      acc.push(name[1]);

      return acc;
    },[]);

    return names;
  },
  'plainList': (string) => {
    if (!string || typeof string !== 'string' || !string.length) throw new Error('plain text parser. Invalid or empty string argument');

    const matches = string.match(regex.asteriks);
    const names = matches.reduce((acc, fullString) => {
      const bracketMatch = fullString.match(regex.hasSquareBrackets);
      const nameMatch = fullString.match(regex.name);

      if (!bracketMatch && !nameMatch) return acc;

      if (bracketMatch && bracketMatch[1]) {
        acc.push(bracketMatch[1]);
      } else if (nameMatch && nameMatch[0]) {
        acc.push(nameMatch[0].trim());
      }

      return acc;
    }, []);

    return names;
  },
  'default': (string) => { // as for now, default duplicates hlist
    if (!string || typeof string !== 'string' || !string.length) throw new Error('default parser. Invalid or empty string argument');

    const matches = string.match(regex.squareBracketsFull);
    const names = matches.reduce((acc, fullNameString) => {
      const name = fullNameString.match(regex.squareBracketsName);

      if (!name || !name[1]) return acc;

      acc.push(name[1]);

      return acc;
    },[]);

    return names;
  }
};

const getParser = (string) => {
  if (!string || typeof string !== 'string' || !string.length) throw new Error('getParser. Invalid or empty string argument');

  let type = 'default';

  if ((regex.hlist).test(string)) type = 'hlist';
  if ((regex.plainList).test(string)) type = 'plainList';

  return parsers[type];
};


module.exports = (data) => {
  if (!data || typeof data !== 'object') throw new Error('parseData. Invalid data');

  return Object.entries(data).reduce((acc, pair) => {
    const [marker, string] = pair;
    const parser = getParser(string);
    const parsedData = parser(string);
    acc[marker] = parsedData;

    return acc;
  }, {});
};