module.exports = (markers) => {
  if (!markers) throw new Error('getRelativesMap. No markers');

  return markers.reduce((acc, marker) => {
    const targetString = `${marker}\\s*=\\s*({{[^}]+}})`;
    const regex = new RegExp(targetString);
    acc[marker] = regex;
  
    return acc;
  }, {});
};