const findMatches = require('./findMatches');
const getRelativesMap = require('./getRelativesMap');
const parseData = require('./parseData');
const hasLink = require('./hasLink');

module.exports = {
  findMatches,
  getRelativesMap,
  parseData,
  hasLink
};